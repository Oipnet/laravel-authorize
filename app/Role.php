<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    const ADMIN_ROLE = 1;
    const MODERTATOR_ROLE = 2;
    const WRITER_ROLE = 3;
}
