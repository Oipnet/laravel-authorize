<?php

namespace App\Policies;

use App\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function moderate ($user)
    {
        foreach ($user->roles as $role) {
            if ($role->id === Role::MODERTATOR_ROLE) {
                return true;
            }
        }
        return false;
    }

    public function write ($user)
    {
        foreach ($user->roles as $role) {
            if ($role->id === Role::WRITER_ROLE) {
                return true;
            }
        }
        return false;
    }
}
