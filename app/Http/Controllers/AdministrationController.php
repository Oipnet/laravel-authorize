<?php

namespace App\Http\Controllers;


class AdministrationController extends Controller
{
    //
    public function index () {
        return view('admin.index');
    }

    public function moderate () {
        return view('admin.moderate');
    }

    public function write () {
        return view('admin.write');
    }
}
