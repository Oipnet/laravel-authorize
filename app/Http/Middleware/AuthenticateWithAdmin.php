<?php
/**
 * Created by PhpStorm.
 * User: arnaud
 * Date: 29/10/16
 * Time: 13:08
 */

namespace App\Http\Middleware;


use App\Role;
use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateWithAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        foreach (Auth::user()->roles as $role) {
            if ($role->id === Role::ADMIN_ROLE) {
                return $next($request);
            }
        }
        return redirect('/home');
    }

}