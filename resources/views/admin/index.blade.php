@extends('../layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Admin panel</div>

                    <div class="panel-body">
                        <ul>
                            @can('moderate', Auth::user())
                                <li><a href="/admin/moderate">Role Moderator</a></li>
                            @endcan
                            @can('write', Auth::user())
                                <li><a href="/admin/write">Role Writer</a></li>
                            @endcan
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
