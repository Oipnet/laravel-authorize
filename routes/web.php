<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::group(['prefix' => 'admin', 'middleware' => 'auth.admin'], function () {
    Route::get('/', 'AdministrationController@index');
    Route::get('/moderate', 'AdministrationController@moderate')->middleware('can:moderate,App\User');
    Route::get('/write', 'AdministrationController@write')->middleware('can:write,App\User');
});
